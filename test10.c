#include<stdio.h>

void add(int *a, int *b,int *s);

void sub(int *a, int *b,int *t);

void mul(int *a, int *b,int *m);

void div(int *a, int *b,int *d);

void rem(int *a, int *b,int *r);

int main()

{

    int num1,num2,sum,di,minus,re,multi;

    printf("Enter two numbers\n");

    scanf("%d %d",&num1,&num2);

    add(&num1,&num2,&sum);

    printf("Sum of 2 numbers is=%d\n",sum);

    sub(&num1,&num2,&minus);

    printf("Subtraction of 2 numbers is=%d\n",minus);

    mul(&num1,&num2,&multi);

    printf("Multiplication of 2 numbers is=%d\n",multi);

    div(&num1,&num2,&di);

    printf("Division of 2 numbers is=%d\n",di);

    rem(&num1,&num2,&re);

    printf("Remainder of 2 numbers is=%d\n",re);

    return 0;

}

void add(int *a, int *b,int *s)

{

    *s=*a+*b;

}

void sub(int *a, int *b,int *t)

{

    *t=*a-*b;

}

void mul(int *a, int *b,int *m)

{

    *m=(*a)*(*b);

}

void div(int *a, int *b,int *d)

{

    *d=(*a)/(*b);

}

void rem(int *a, int *b,int *r)

{

    *r=(*a)%(*b);

}