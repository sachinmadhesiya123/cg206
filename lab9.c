#include<stdio.h>

int input()

{

           int a;

           printf("Enter any number :\n");

           scanf("%d",&a);

           return a;

}

void swap(int *x,int *y)

{

           int temp;

           temp= *x;

           *x= *y;

           *y=temp;

}

int main()

{

           int n1,n2;

           n1=input();

           n2=input();

           printf("Before swapping:\n n1=%d\n n2=%d\n",n1,n2);

           swap(&n1,&n2);

           printf("After Swapping:\n n1=%d\n n2=%d\n",n1,n2);

           return 0;

}

 